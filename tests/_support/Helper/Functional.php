<?php

/**
 * Upspring Hosting plugin for Craft CMS 5.x
 *
 * Works with Upspring's hosting platform to optimize performance and security
 *
 * @link      https://upspringdigital.com/
 * @copyright Copyright (c) 2021 Upspring
 */

namespace Helper;

use Codeception\Module;

/**
 * Class Functional
 *
 * Here you can define custom actions.
 * All public methods declared in helper class will be available in $I
 */
class Functional extends Module
{
}
