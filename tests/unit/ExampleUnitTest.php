<?php

/**
 * Upspring Hosting plugin for Craft CMS 5.x
 *
 * Works with Upspring's hosting platform to optimize performance and security
 *
 * @link      https://upspringdigital.com/
 * @copyright Copyright (c) 2021 Upspring
 */

namespace upspring\upspringhostingtests\unit;

use Codeception\Test\Unit;
use UnitTester;
use Craft;
use upspring\upspringhosting\UpspringHosting;

/**
 * ExampleUnitTest
 *
 *
 * @author    Upspring
 * @package   UpspringHosting
 * @since     1.0.0
 */
class ExampleUnitTest extends Unit
{
    // Properties
    // =========================================================================

    /**
     * @var UnitTester
     */
    protected $tester;

    // Public methods
    // =========================================================================

    // Tests
    // =========================================================================

    /**
     *
     */
    public function testPluginInstance()
    {
        $this->assertInstanceOf(
            UpspringHosting::class,
            UpspringHosting::$plugin
        );
    }

    /**
     *
     */
    public function testCraftEdition()
    {
        Craft::$app->setEdition(Craft::Pro);

        $this->assertSame(
            Craft::Pro,
            Craft::$app->getEdition()
        );
    }
}
