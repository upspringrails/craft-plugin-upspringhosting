# Upspring Hosting plugin for Craft CMS 5

Works with Upspring's hosting platform to optimize performance and security

Currently, the only feature implemented is OIDC auth powered by the Upspring Hosting Dashboard (dashboard.upspringhosting.net).

## Known issues

* Although SSO allows you to log in successfully, Craft occasionally prompts for a password for certain actions, or when your session expires. Since users created via SSO will not typically have a password, there isn't much you can do with that prompt at the moment. We are looking for a way to add an SSO button to this prompt, but in the meantime, you can either manually reload the page and log in again, or set a password on your account if you run into this issue often and want to avoid having to reload.

## Requirements

This plugin requires Craft CMS 5.x.

For Craft 4.x, use the [craft4 branch](https://bitbucket.org/upspringrails/craft-plugin-upspringhosting/branch/craft4).
For Craft 3.x, use the [craft3 branch](https://bitbucket.org/upspringrails/craft-plugin-upspringhosting/branch/craft3).

## Installation

To install the plugin, follow these instructions.

1. Add bitbucket repo to composer repositories:

        composer config repositories.upspringhosting vcs https://bitbucket.org/upspringrails/craft-plugin-upspringhosting.git

2. Composer require:

        composer require upspring/upspring-hosting:dev-craft5

3. Set the following variables in .env:

        UPSPRING_SSO_OAUTH_CLIENT_ID=
        UPSPRING_SSO_OAUTH_CLIENT_SECRET=
        UPSPRING_SSO_OAUTH_REDIRECT_URI=http://[your-domain]/admin/sso

The Client ID and Secret can be found in the Hosting Dashboard under Tools > Configure CMS Plugin for the site in question.

(If `cpTrigger` has been changed, then replace `admin` with the correct value.)

4. In the Control Panel, go to Settings → Plugins and click the “Install” button for Upspring Hosting, or use command line:

        php craft plugin/install upspring-hosting
