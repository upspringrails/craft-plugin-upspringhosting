<?php

/**
 * Upspring Hosting plugin for Craft CMS 5.x
 *
 * Works with Upspring's hosting platform to optimize performance and security
 *
 * @link      https://upspringdigital.com/
 * @copyright Copyright (c) 2021 Upspring
 */

/**
 * Upspring Hosting en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('upspring-hosting', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Upspring
 * @package   UpspringHosting
 * @since     1.0.0
 */
return [
    'Upspring Hosting plugin loaded' => 'Upspring Hosting plugin loaded',
];
