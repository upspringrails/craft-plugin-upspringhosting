<?php

/**
 * Upspring Hosting plugin for Craft CMS 5.x
 *
 * Works with Upspring's hosting platform to optimize performance and security
 *
 * @link      https://upspringdigital.com/
 * @copyright Copyright (c) 2021 Upspring
 */

namespace upspring\upspringhosting\services;

use upspring\upspringhosting\UpspringHosting;

use Craft;
use craft\base\Component;

/**
 * UpspringHostingService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Upspring
 * @package   UpspringHosting
 * @since     1.0.0
 */
class UpspringHostingService extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     UpspringHosting::$plugin->upspringHostingService->exampleService()
     *
     * @return mixed
     */
    public function exampleService()
    {
        $result = 'something';

        return $result;
    }
}
