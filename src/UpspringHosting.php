<?php

/**
 * Upspring Hosting plugin for Craft CMS 5.x
 *
 * Works with Upspring's hosting platform to optimize performance and security
 *
 * @link      https://upspringdigital.com/
 * @copyright Copyright (c) 2022 Upspring
 */

namespace upspring\upspringhosting;

use upspring\upspringhosting\services\UpspringHostingService as UpspringHostingServiceService;
use upspring\upspringhosting\variables\UpspringHostingVariable;
use upspring\upspringhosting\utilities\UpspringHostingUtility as UpspringHostingUtilityUtility;
use upspring\upspringhosting\widgets\UpspringHostingWidget as UpspringHostingWidgetWidget;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\console\Application as ConsoleApplication;
use craft\web\UrlManager;
use craft\web\View;
use craft\services\Utilities;
use craft\web\twig\variables\Cp;
use craft\web\twig\variables\CraftVariable;
use craft\services\Dashboard;
use craft\events\RegisterComponentTypesEvent;
use craft\events\RegisterCpNavItemsEvent;
use craft\events\RegisterUrlRulesEvent;
use craft\events\TemplateEvent;

use yii\base\Event;

/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://docs.craftcms.com/v3/extend/
 *
 * @author    Upspring
 * @package   UpspringHosting
 * @since     1.0.0
 *
 * @property  UpspringHostingServiceService $upspringHostingService
 */
class UpspringHosting extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * Static property that is an instance of this plugin class so that it can be accessed via
     * UpspringHosting::$plugin
     *
     * @var UpspringHosting
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * To execute your plugin’s migrations, you’ll need to increase its schema version.
     *
     * @var string
     */
    public string $schemaVersion = '1.0.0';

    /**
     * Set to `true` if the plugin should have a settings view in the control panel.
     *
     * @var bool
     */
    public bool $hasCpSettings = false;

    /**
     * Set to `true` if the plugin should have its own section (main nav item) in the control panel.
     *
     * @var bool
     */
    public bool $hasCpSection = false;

    // Public Methods
    // =========================================================================

    /**
     * Set our $plugin static property to this class so that it can be accessed via
     * UpspringHosting::$plugin
     *
     * Called after the plugin class is instantiated; do any one-time initialization
     * here such as hooks and events.
     *
     * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
     * you do not need to load it in your init() method.
     *
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        // Add in our console commands
        // if (Craft::$app instanceof ConsoleApplication) {
        //     $this->controllerNamespace = 'upspring\upspringhosting\console\controllers';
        // }

        // Register our site routes
        // Event::on(
        //     UrlManager::class,
        //     UrlManager::EVENT_REGISTER_SITE_URL_RULES,
        //     function (RegisterUrlRulesEvent $event) {
        //         $event->rules['sso'] = 'upspring-hosting/default/sso';
        //     }
        // );

        // Event::on(
        //     Cp::class,
        //     Cp::EVENT_REGISTER_CP_NAV_ITEMS,
        //     function (RegisterCpNavItemsEvent $event) {
        //         $event->navItems[] = [
        //             'url' => '/admin/upspring-hosting',
        //             'label' => 'Upspring',
        //             // 'icon' => '@mynamespace/path/to/icon.svg',
        //             'badgeCount' => 1,
        //             'subnav' => [
        //                 'foo' => ['label' => 'Foo', 'url' => 'admin/upspring-hosting'],
        //                 'bar' => ['label' => 'Bar', 'url' => 'admin/upspring-hosting', 'badgeCount' => 1],
        //             ],
        //         ];
        //     }
        // );

        // Register our CP routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_CP_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['sso'] = 'upspring-hosting/default/sso';
            }
        );

        // Inject a button into the login template
        Event::on(
            View::class,
            View::EVENT_AFTER_RENDER_TEMPLATE,
            function (TemplateEvent $event) {
                if ($event->template === 'login.twig') {
                    // Inject SSO button using JavaScript
                    $event->output .= '
<script>
  document.addEventListener("DOMContentLoaded", function() {
    const signInButton = document.querySelector(".login-form button[type=submit]");
    if (signInButton) {
        // create a button container
        const buttonContainer = document.createElement("div");
        buttonContainer.style.display = "flex";
        buttonContainer.style.flexDirection = "column";
        buttonContainer.style.gap = "8px";

        signInButton.parentNode.insertBefore(buttonContainer, signInButton);

        const ssoButton = document.createElement("a");
        ssoButton.href = "sso";
        ssoButton.className = "btn";
        ssoButton.textContent = "Login with Upspring SSO";

        buttonContainer.appendChild(signInButton);
        buttonContainer.appendChild(ssoButton);
    }
  });
</script>';
                }
            }
        );

        // Register our utilities
        // Event::on(
        //     Utilities::class,
        //     Utilities::EVENT_REGISTER_UTILITY_TYPES,
        //     function (RegisterComponentTypesEvent $event) {
        //         $event->types[] = UpspringHostingUtilityUtility::class;
        //     }
        // );

        // Register our widgets
        // Event::on(
        //     Dashboard::class,
        //     Dashboard::EVENT_REGISTER_WIDGET_TYPES,
        //     function (RegisterComponentTypesEvent $event) {
        //         $event->types[] = UpspringHostingWidgetWidget::class;
        //     }
        // );

        // Register our variables
        // Event::on(
        //     CraftVariable::class,
        //     CraftVariable::EVENT_INIT,
        //     function (Event $event) {
        //         /** @var CraftVariable $variable */
        //         $variable = $event->sender;
        //         $variable->set('upspringHosting', UpspringHostingVariable::class);
        //     }
        // );

        // Do something after we're installed
        // Event::on(
        //     Plugins::class,
        //     Plugins::EVENT_AFTER_INSTALL_PLUGIN,
        //     function (PluginEvent $event) {
        //         if ($event->plugin === $this) {
        //             // We were just installed
        //         }
        //     }
        // );

        /**
         * Logging in Craft involves using one of the following methods:
         *
         * Craft::trace(): record a message to trace how a piece of code runs. This is mainly for development use.
         * Craft::info(): record a message that conveys some useful information.
         * Craft::warning(): record a warning message that indicates something unexpected has happened.
         * Craft::error(): record a fatal error that should be investigated as soon as possible.
         *
         * Unless `devMode` is on, only Craft::warning() & Craft::error() will log to `craft/storage/logs/web.log`
         *
         * It's recommended that you pass in the magic constant `__METHOD__` as the second parameter, which sets
         * the category to the method (prefixed with the fully qualified class name) where the constant appears.
         *
         * To enable the Yii debug toolbar, go to your user account in the AdminCP and check the
         * [] Show the debug toolbar on the front end & [] Show the debug toolbar on the Control Panel
         *
         * http://www.yiiframework.com/doc-2.0/guide-runtime-logging.html
         */
        Craft::info(
            Craft::t(
                'upspring-hosting',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================

}
