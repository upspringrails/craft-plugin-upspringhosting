<?php

/**
 * Upspring Hosting plugin for Craft CMS 5.x
 *
 * Works with Upspring's hosting platform to optimize performance and security
 *
 * @link      https://upspringdigital.com/
 * @copyright Copyright (c) 2021 Upspring
 */

namespace upspring\upspringhosting\controllers;

use upspring\upspringhosting\UpspringHosting;

use Craft;
use craft\controllers\UsersController;
use craft\events\LoginFailureEvent;
use craft\helpers\App;
use craft\helpers\UrlHelper;
use craft\web\Controller;

/**
 * Default Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Upspring
 * @package   UpspringHosting
 * @since     1.0.0
 */
class DefaultController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    array|bool|int Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected array|bool|int $allowAnonymous = ['login', 'sso'];

    // Public Methods
    // =========================================================================

    /**
     * Handle a request going to our plugin's index action URL,
     * e.g.: actions/upspring-hosting/default
     *
     * @return mixed
     */
    public function actionLogin()
    {
        // render our custom login template (with the SSO button)
        return $this->renderTemplate('upspring-hosting/_cp/login');
    }

    /**
     * Handle a request going to our plugin's actionDoSomething URL,
     * e.g.: actions/upspring-hosting/default/do-something
     *
     * @return mixed
     */
    public function actionSso()
    {
        $userinfo = $this->_getAuthorizationCode();
        if (!$userinfo) return "OIDC error";

        $name = $userinfo["full_name"];
        $username = $userinfo["preferred_username"];
        $email = $userinfo["email"];

        // first, try to find a matching user
        $user = \craft\elements\User::find()->where(['username' => $username, 'email' => $email])->one();

        // otherwise, create new user
        if (!$user) {
            $user = new \craft\elements\User();

            $parts = explode(' ', $name, 2);
            $user->firstName = $parts[0];
            $user->lastName = $parts[1] ?? "";
            $user->username = $username;
            $user->email = $email;
            $user->admin = true;
            $user->active = true;

            if ($user->validate(null, false)) {
                Craft::$app->elements->saveElement($user, false);
            }
        }

        // if the find or create succeeded, log in
        if ($user->id) {
            // Get the session duration
            $generalConfig = Craft::$app->getConfig()->getGeneral();
            $duration = $generalConfig->userSessionDuration;

            // Try logging them in
            $success = Craft::$app->user->login($user, $duration);

            if ($success) {
                return $this->_handleSuccessfulLogin();
            } else {
                // Unknown error
                $this->_handleLoginFailure($user->authError, $user);
                return "Unknown auth error";
            }
        } else {
            return "An error occurred, most likely a conflict with your username or email address.";
        }
    }

    private function _getAuthorizationCode()
    {
        $clientId = App::env('UPSPRING_SSO_OAUTH_CLIENT_ID');
        $clientSecret = App::env('UPSPRING_SSO_OAUTH_CLIENT_SECRET');
        $redirectUri = App::env('UPSPRING_SSO_OAUTH_REDIRECT_URI');

        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId'                => $clientId,
            'clientSecret'            => $clientSecret,
            'scopes'                  => 'email profile openid',
            'redirectUri'             => $redirectUri,
            'urlAuthorize'            => 'https://dashboard.upspringhosting.net/oauth/authorize',
            'urlAccessToken'          => 'https://dashboard.upspringhosting.net/oauth/token',
            'urlResourceOwnerDetails' => 'https://dashboard.upspringhosting.net/oauth/userinfo'
        ]);

        // If we don't have an authorization code then get one
        if (!isset($_GET['code'])) {
            // Fetch the authorization URL from the provider; this returns the
            // urlAuthorize option and generates and applies any necessary parameters
            // (e.g. state).
            $authorizationUrl = $provider->getAuthorizationUrl();

            // Get the state generated for you and store it to the session.
            $_SESSION['oauth2state'] = $provider->getState();

            // Redirect the user to the authorization URL.
            header('Location: ' . $authorizationUrl);
            exit;

            // Check given state against previously stored one to mitigate CSRF attack
        } elseif (empty($_GET['state']) || (isset($_SESSION['oauth2state']) && $_GET['state'] !== $_SESSION['oauth2state'])) {
            if (isset($_SESSION['oauth2state'])) {
                unset($_SESSION['oauth2state']);
            }

            exit('Invalid state');
        } else {
            try {
                // Try to get an access token using the authorization code grant.
                $accessToken = $provider->getAccessToken('authorization_code', [
                    'code' => $_GET['code']
                ]);

                // We have an access token, which we may use in authenticated
                // requests against the service provider's API.
                // echo 'Access Token: ' . $accessToken->getToken() . "<br>";
                // echo 'Refresh Token: ' . $accessToken->getRefreshToken() . "<br>";
                // echo 'Expired in: ' . $accessToken->getExpires() . "<br>";
                // echo 'Already expired? ' . ($accessToken->hasExpired() ? 'expired' : 'not expired') . "<br>";

                // Using the access token, we may look up details about the
                // resource owner.
                $resourceOwner = $provider->getResourceOwner($accessToken);

                // var_export($resourceOwner->toArray());

                // The provider provides a way to get an authenticated API request for
                // the service, using the access token; it returns an object conforming
                // to Psr\Http\Message\RequestInterface.
                // $request = $provider->getAuthenticatedRequest(
                //     'GET',
                //     'https://service.example.com/resource',
                //     $accessToken
                // );

                return $resourceOwner->toArray();
            } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
                // Failed to get the access token or user details.
                exit($e->getMessage());
            }
        }
    }

    /**
     * Redirects the user after a successful login attempt, or if they visited the Login page while they were already
     * logged in.
     *
     * @return Response
     */
    private function _handleSuccessfulLogin(): \yii\web\Response
    {
        // Get the return URL
        $userSession = Craft::$app->getUser();
        $defaultUrl = UrlHelper::cpUrl(Craft::$app->getConfig()->getGeneral()->getPostCpLoginRedirect());
        $returnUrl = $userSession->getReturnUrl($defaultUrl);

        // Clear it out
        $userSession->removeReturnUrl();

        // If this was an Ajax request, just return success:true
        if ($this->request->getAcceptsJson()) {
            $return = [
                'success' => true,
                'returnUrl' => $returnUrl,
            ];

            if (Craft::$app->getConfig()->getGeneral()->enableCsrfProtection) {
                $return['csrfTokenValue'] = $this->request->getCsrfToken();
            }

            return $this->asJson($return);
        }

        return $this->redirectToPostedUrl($userSession->getIdentity(), $returnUrl);
    }

    /**
     * @param string $authError
     * @param UserElement $user
     */
    private function _handleLoginFailure(string $authError = null, \craft\elements\User $user = null)
    {
        $message = \craft\helpers\User::getLoginFailureMessage($authError, $user);

        // Fire a 'loginFailure' event
        $event = new LoginFailureEvent([
            'authError' => $authError,
            'message' => $message,
            'user' => $user,
        ]);
        $this->trigger(UsersController::EVENT_LOGIN_FAILURE, $event);
    }
}
