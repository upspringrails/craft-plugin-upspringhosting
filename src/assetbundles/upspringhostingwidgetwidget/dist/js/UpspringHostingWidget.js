/**
 * Upspring Hosting plugin for Craft CMS
 *
 * UpspringHostingWidget Widget JS
 *
 * @author    Upspring
 * @copyright Copyright (c) 2021 Upspring
 * @link      https://upspringdigital.com/
 * @package   UpspringHosting
 * @since     1.0.0
 */
