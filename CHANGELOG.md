# Upspring Hosting Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## 1.1.0 - 2023-03-24
### Added
- "Login with Upspring SSO" button

## 1.0.0 - 2021-11-21
### Added
- Initial release
